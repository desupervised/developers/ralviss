# RAlviss <img src='man/figures/logo.png' align="right" height="139" />

[![pipeline status](https://gitlab.com/desupervised/developers/ralviss/badges/master/pipeline.svg)](https://gitlab.com/desupervised/developers/ralviss/commits/master)
[![coverage report](https://gitlab.com/desupervised/developers/ralviss/badges/master/coverage.svg)](https://gitlab.com/desupervised/developers/ralviss/commits/master)
[![documentation](https://img.shields.io/badge/documentation-dev-blue.svg)](https://desupervised.gitlab.io/developers/ralviss/docs/)
[![Lifecycle: maturing](https://img.shields.io/badge/lifecycle-maturing-blue.svg)](https://www.tidyverse.org/lifecycle/#maturing)

This is an R package making it possible to interface with Alvíss AI through R.

Please note that the 'ralviss' project is released with a
[Contributor Code of Conduct](CODE_OF_CONDUCT.md).
By contributing to this project, you agree to abide by its terms.
# ralviss 0.6.1

* Updated orcid
* Fixed prediction function in new Time series 1.3.1 format

# ralviss 0.6.0

## Features

* Added possibility to delete data
* Added possibility to delete briefs

# ralviss 0.5.0

## Features

* Introduced more badges and documentation

# ralviss 0.4.0

## BUGS

* None

## Features

* Added Joblog function to check status of running jobs

# ralviss 0.3.0

# ralviss 0.2.1

# ralviss 0.2.0

# ralviss 0.1.1

# ralviss 0.1.0

* Added a `NEWS.md` file to track changes to the package.
